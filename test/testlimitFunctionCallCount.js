const {limitFunctionCallCount} = require('../limitFunctionCallCount.js');

function myFunction(){
    console.log('Hello world!');
}
const result = limitFunctionCallCount(myFunction, 2);
result();
result();
result();

function wrapper(initialValue = 0){
    let currentValue = initialValue;
    const methods = {
        increment: function(incrementValue = 1) {
            currentValue += incrementValue;
            return currentValue;
        }, 
        decrement: function(decrementValue = 1) {
            currentValue -= decrementValue;
            return currentValue;
        }
    };
    return methods;
}

module.exports = {wrapper};
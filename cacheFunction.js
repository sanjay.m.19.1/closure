function cacheResults(callBackFunction){
    resultsBuffer = {};
    function retriveIfCached(){
        if(!callBackFunction) return undefined;
        let resultForKey = JSON.stringify(arguments);
        if(!resultsBuffer.hasOwnProperty(resultForKey)){
            resultsBuffer[resultForKey] = callBackFunction(...Object.values(arguments));
            //console.log('called call back');
        }
        //console.log(resultsBuffer[resultForKey]);
        return resultsBuffer[resultForKey];
    }
    return retriveIfCached;
}

module.exports = {cacheResults};
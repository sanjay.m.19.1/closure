function limitFunctionCallCount(callBackFunction, callLimit = 0){
    if(!callBackFunction || !callLimit) return;
    function functionCallCounter(){
        if(callLimit){
            callLimit--;
            callBackFunction();
        } else{
            console.log('limit exceeded');
            return null;
        }
    }
    return functionCallCounter;
}

module.exports = {limitFunctionCallCount};